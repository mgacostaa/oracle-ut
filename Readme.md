# Oracle UT - Advance SQL Queries
_Marlon Acosta, Dic 2021_


## Cheking database ER chart with DBeaver

## [Aggregate Functions](./aggregate_functions.md)
  * DISTINCT
  * GROUP BY
  * HAVING
  * MIN
  * MAX
  * SUM
  * AVG
  * COUNT
  * RANK

https://docs.oracle.com/database/121/SQLRF/functions003.htm#SQLRF20035

## [Single-Row Functions](./single-row-functions.md)

* String-Manipulation Functions=> https://www.oracletutorial.com/oracle-string-functions/
  * CONCAT
  * DUMP
  * INITCAP
  * INSTR
  * LOWER
  * LTRIM
  * RTRIM
  * TRIM
  * REPLACE
  * SUBSTR


* Functions for Date and Time Manipulation => https://www.oracletutorial.com/oracle-date-functions/
  * EXTRACT
  * TRUNC
  * MONTHS_BETWEEN
  * ADD_MONTHS
  * SYSDATE
  * SYSTIMESTAMP
  * TO_CHAR
  * TO_DATE


* CASE WHEN => https://www.oracletutorial.com/oracle-basics/oracle-case/

## [Joins](joins.md)

* INNER JOIN, CROSS JOIN, LEFT, RIGHT, and FULL OUTER JOIN => https://www.oracletutorial.com/oracle-basics/oracle-joins/

## [Using the Set Operators](./unions.md)
* UNION and UNION ALL, INTERSECT, MINUS =>  => https://www.geeksforgeeks.org/difference-between-join-and-union-in-sql/

## Building Subqueries

* Multiple Row Subqueries
* Subqueries factoring => https://oracle-base.com/articles/misc/with-clause

## Views


## [Setup](./setup.md)

For the setup you need to download this repository in your local machine.

The scripts to run are:

* `init_db.sql` => creates database and tablespace (file in your os with .tbs extension) in your local oracle xe installation
*` reset_db.sql` => deletes database and tablespace (file in your os with .tbs extension) in your local oracle xe installation
* `load_test_data.sh` (this runs only in bash / git console) => You can install git bash from here: https://git-scm.com/download/win
* `Test_Data.sql` => random data to create fake calls into the IVR database

Steps, order to execution:

0. Go to the folder of the scripts `oracle-ut/db/` and run sqlplus from that with `sqlplus / as sysdba`
0. Initialize database and tablespace => `start init_db.sql`
0. Change the values for login on `start init_db.sql` and `Test_Data.sql`
0. Config the limit times you want to run the script (each iteration will be mock one call)
0. Open git bash on the scripts folder and run => `./load_test_data.sh` and go for a coffe...
0. Connect vía your preferred DBMA


# Credits

* Content Source => https://www.roitraining.com/course-264-advanced-sql-queries-oracle-databases-hands/
